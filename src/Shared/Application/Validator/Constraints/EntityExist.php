<?php

namespace App\Shared\Application\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\MissingOptionsException;

/**
 * @Annotation
 */
final class EntityExist extends Constraint
{
    public $message = 'This value is already used.';

    /**
     * Full entity className
     */
    public string $className;

    /**
     * Field name to validate
     */
    public string $fieldName;

    /**
     * validation field
     */
    public string $validationField = '';

    public function __construct($options = null)
    {
        if (!isset($options['fieldName']))
            throw new MissingOptionsException(sprintf('Missing required option: %s' , 'fieldName'), $options);

        $this->fieldName = $options['fieldName'];

        if (!isset($options['className']))
            throw new MissingOptionsException(sprintf('Missing required option: %s' , 'className'), $options);

        $this->className = $options['className'];

        parent::__construct($options);
    }
}
