<?php


namespace App\Shared\Infrastructure;

use Doctrine\Persistence\ObjectRepository;

interface EntityRepositoryInterface extends ObjectRepository
{
    /**
     * @param object $entityObject
     */
    public function persist(object $entityObject): void;

    /**
     * @param object $entityObject
     */
    public function remove(object $entityObject): void;
}