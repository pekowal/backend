<?php


namespace App\Shared\Infrastructure;


use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

final class ValidationErrorsFetcher
{
    private ConstraintViolationListInterface $constraintViolationList;

    public function __construct(ConstraintViolationListInterface $constraintViolationList) {

        $this->constraintViolationList = $constraintViolationList;
    }

    /**
     * @return array|string[]
     */
    public function getErrorsMessages(): array
    {
        $errors = [];

        /** @var ConstraintViolationInterface $violation */
        foreach ($this->constraintViolationList as $violation) {
            $errors[$violation->getPropertyPath()] = $violation->getMessage();
        }

        return $errors;
    }
}