<?php


namespace App\Shared\Infrastructure;


use App\Account\Domain\Entity\Profile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

abstract class AbstractEntityRepository extends ServiceEntityRepository implements EntityRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(ManagerRegistry $registry, $entityClass, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, $entityClass);
        $this->entityManager = $entityManager;
    }


    public function persist(object $entityObject): void
    {
        $this->entityManager->persist($entityObject);
    }

    /**
     * @var Profile|object $profile
     */
    public function remove(object $profile): void
    {
        $this->entityManager->remove($profile);
    }

}