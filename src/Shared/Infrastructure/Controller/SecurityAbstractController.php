<?php


namespace App\Shared\Infrastructure\Controller;


use App\Account\Domain\Entity\Account;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class SecurityAbstractController extends AbstractController
{
    protected function getLoggedAccount(): ?Account
    {
        /** @var Account $account */
        $account = parent::getUser();

        return $account;
    }
}