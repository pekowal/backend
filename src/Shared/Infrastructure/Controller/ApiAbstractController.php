<?php


namespace App\Shared\Infrastructure\Controller;


use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Serializer\SerializerInterface;

abstract class ApiAbstractController extends SecurityAbstractController
{
    protected SerializerInterface $serializer;

    //TODO remove
    protected MessageBusInterface $messageBus;
    protected MessageBusInterface $commandBud;
    protected MessageBusInterface $queryBus;

    public function __construct(SerializerInterface $serializer, MessageBusInterface $commandBud, MessageBusInterface $queryBus)
    {
        $this->serializer = $serializer;
        $this->messageBus = $commandBud;
        $this->commandBud = $commandBud;
        $this->queryBus = $queryBus;
    }

}