<?php

namespace App\Shared\DataFixtures;

use App\Account\Domain\Entity\Account;
use App\Account\Domain\Entity\Personality;
use App\Account\Domain\Entity\Profile;
use App\Calendar\Domain\Entity\Calendar;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

//TODO move to Account folder
class AccountFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {

        $account = new Account('pekowal@gmail.com', '123123');
        $account->grantExpertRole();
        $account->grantAdminRole();
        $pass = $this->passwordEncoder->encodePassword($account, '123123');
        $account->setPassword($pass);

        $personality = new Personality('Przemek', 'Kowalczyk');
        $profile = new Profile($account, $personality, new Calendar());
        $manager->persist($account);
        $manager->persist($profile);
        $manager->persist($personality);

        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
