<?php


namespace App\Calendar\Application\QueryHandler;

use App\Calendar\Application\Query\FetchVacationsQuery;
use App\Calendar\Domain\Entity\Schedule;
use App\Calendar\Infrastructure\Repository\VacationRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class FetchVacationsQueryHandler implements MessageHandlerInterface
{
    /**
     * @var VacationRepository
     */
    private VacationRepository $vacationRepository;

    public function __construct(VacationRepository $vacationRepository)
    {
        $this->vacationRepository = $vacationRepository;
    }

    /**
     * @param FetchVacationsQuery $query
     * @return Schedule[]
     */
    public function __invoke(FetchVacationsQuery $query): array
    {
        return $this->vacationRepository->fetchVacations($query->getProfileId());
    }
}