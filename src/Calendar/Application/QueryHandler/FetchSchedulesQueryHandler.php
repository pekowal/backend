<?php


namespace App\Calendar\Application\QueryHandler;


use App\Calendar\Application\Query\FetchSchedulesQuery;
use App\Calendar\Domain\Entity\Schedule;
use App\Calendar\Infrastructure\Repository\ScheduleRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class FetchSchedulesQueryHandler implements MessageHandlerInterface
{
    /**
     * @var ScheduleRepository
     */
    private ScheduleRepository $scheduleRepository;

    public function __construct(ScheduleRepository $scheduleRepository)
    {
        $this->scheduleRepository = $scheduleRepository;
    }

    /**
     * @param FetchSchedulesQuery $query
     * @return Schedule[]
     */
    public function __invoke(FetchSchedulesQuery $query): array
    {
        return $this->scheduleRepository->fetchSchedules($query->getProfileId());
    }
}