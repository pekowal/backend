<?php


namespace App\Calendar\Application\QueryHandler;

use App\Calendar\Application\Query\FetchExpertAppointmentsQuery;
use App\Calendar\Application\Query\FetchVacationsQuery;
use App\Calendar\Domain\Entity\Schedule;
use App\Calendar\Infrastructure\Repository\AppointmentRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class FetchAppointmentsQueryHandler implements MessageHandlerInterface
{
    /**
     * @var AppointmentRepository
     */
    private AppointmentRepository $appointmentRepository;

    public function __construct(AppointmentRepository $appointmentRepository)
    {
        $this->appointmentRepository = $appointmentRepository;
    }

    /**
     * @param FetchExpertAppointmentsQuery $query
     * @return Schedule[]
     */
    public function __invoke(FetchExpertAppointmentsQuery $query): array
    {
        return $this->appointmentRepository->fetchAppointments($query->getProfileId());
    }
}