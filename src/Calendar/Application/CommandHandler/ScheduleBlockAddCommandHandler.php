<?php


namespace App\Calendar\Application\CommandHandler;


use App\Calendar\Application\Command\ScheduleBlockAddCommand;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ScheduleBlockAddCommandHandler implements MessageHandlerInterface
{
    public function __invoke(ScheduleBlockAddCommand $scheduleAddCommand): void
    {
        return;
    }
}