<?php


namespace App\Calendar\Application\CommandHandler;


use App\Account\Domain\Entity\Profile;
use App\Account\Infrastructure\Repository\ProfileRepository;
use App\Calendar\Application\Command\VacationAddCommand;
use App\Calendar\Domain\Entity\Vacation;
use App\Calendar\Infrastructure\Repository\VacationRepository;
use DateTime;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class VacationAddCommandHandler implements MessageHandlerInterface
{
    /**
     * @var VacationRepository
     */
    private VacationRepository $vacationRepository;
    /**
     * @var ProfileRepository
     */
    private ProfileRepository $profileRepository;

    public function __construct(VacationRepository $vacationRepository, ProfileRepository $profileRepository)
    {
        $this->vacationRepository = $vacationRepository;
        $this->profileRepository = $profileRepository;
    }

    public function __invoke(VacationAddCommand $command): Vacation
    {
        /** @var Profile $profile */
        $profile = $this->profileRepository->find($command->getProfileId());
        $vacation = new Vacation($profile, new DateTime($command->getStartDate()), new DateTime($command->getEndDate()), $command->getName());

        $this->vacationRepository->persist($vacation);

        return $vacation;
    }
}