<?php


namespace App\Calendar\Application\CommandHandler;

use App\Account\Domain\Entity\Profile;
use App\Account\Infrastructure\Repository\AccountRepository;
use App\Account\Infrastructure\Repository\ProfileRepository;
use App\Calendar\Application\Command\AppointmentCancelCommand;
use App\Calendar\Domain\Entity\Appointment;
use App\Calendar\Infrastructure\Repository\AppointmentRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class AppointmentCreateCommandHandler implements MessageHandlerInterface
{
    /**
     * @var AppointmentRepository
     */
    private AppointmentRepository $appointmentRepository;
    /**
     * @var ProfileRepository
     */
    private ProfileRepository $profileRepository;
    /**
     * @var AccountRepository
     */
    private AccountRepository $accountRepository;

    public function __construct(AppointmentRepository $appointmentRepository, ProfileRepository $profileRepository, AccountRepository $accountRepository)
    {
        $this->appointmentRepository = $appointmentRepository;
        $this->profileRepository = $profileRepository;
        $this->accountRepository = $accountRepository;
    }

    public function __invoke(AppointmentCancelCommand $appointmentCreateCommand): void
    {
        /** @var Profile $expertProfile */
        $expertProfile = $this->profileRepository->find($appointmentCreateCommand->getExpertProfileId());
        $clientAccount = $this->accountRepository->find($appointmentCreateCommand->getClientAccountId());
        $appointment = new Appointment($expertProfile, $clientAccount);

        $this->appointmentRepository->persist($appointment);
    }
}