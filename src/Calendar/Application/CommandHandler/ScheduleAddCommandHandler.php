<?php


namespace App\Calendar\Application\CommandHandler;

use App\Account\Domain\Entity\Profile;
use App\Account\Infrastructure\Repository\ProfileRepository;
use App\Calendar\Application\AsyncCommand\RecalculateScheduleBlocksCommand;
use App\Calendar\Application\Command\ScheduleAddCommand;
use App\Calendar\Domain\Entity\Schedule;
use App\Calendar\Infrastructure\Utils\ScheduleDayHelper;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class ScheduleAddCommandHandler implements MessageHandlerInterface
{
    //TODO move
    private const WEEK_DAYS = [
      'Poniedziałek',
      'Wtorek',
      'Środa',
      'Czwartek',
      'Piątek',
      'Sobota',
      'Niedziela',
    ];

    private ProfileRepository $profileRepository;
    private MessageBusInterface $messageBus;

    public function __construct(ProfileRepository $profileRepository, MessageBusInterface $asyncBus)
    {
        $this->profileRepository = $profileRepository;
        $this->messageBus = $asyncBus;
    }

    public function __invoke(ScheduleAddCommand $scheduleAddCommand): Schedule
    {
        /** @var Profile $profile */
        $profile = $this->profileRepository->find($scheduleAddCommand->getProfileId());

        $scheduleEntity = new Schedule(
            $profile->getCalendar(),
            $scheduleAddCommand->getWeekDay(),
            self::WEEK_DAYS[$scheduleAddCommand->getWeekDay()],
            ScheduleDayHelper::convertTimeToHours($scheduleAddCommand->getStartDate()),
            ScheduleDayHelper::convertTimeToHours($scheduleAddCommand->getEndDate()),
        );

        $this->profileRepository->persist($scheduleEntity);

        $this->messageBus->dispatch(new RecalculateScheduleBlocksCommand($profile->getCalendar()->getId()));

        return $scheduleEntity;
    }


}