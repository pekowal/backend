<?php


namespace App\Calendar\Application\CommandHandler;


use App\Calendar\Application\Command\VacationRemoveCommand;
use App\Calendar\Domain\Entity\Vacation;
use App\Calendar\Infrastructure\Repository\VacationRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class VacationRemoveCommandHandler implements MessageHandlerInterface
{
    /**
     * @var VacationRepository
     */
    private VacationRepository $vacationRepository;

    public function __construct(VacationRepository $vacationRepository)
    {
        $this->vacationRepository = $vacationRepository;
    }

    public function __invoke(VacationRemoveCommand $command): void
    {
        /** @var Vacation $vacation */
        $vacation = $this->vacationRepository->find($command->getVacationId());

        $this->vacationRepository->remove($vacation);
    }
}