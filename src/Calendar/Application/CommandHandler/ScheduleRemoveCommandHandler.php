<?php


namespace App\Calendar\Application\CommandHandler;


use App\Calendar\Application\Command\ScheduleRemoveCommand;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ScheduleRemoveCommandHandler implements MessageHandlerInterface
{
    public function __invoke(ScheduleRemoveCommand $command): void
    {
        return;
    }
}