<?php


namespace App\Calendar\Application\CommandHandler;


use App\Calendar\Application\Command\CleanCalendarCommand;
use App\Calendar\Infrastructure\Repository\ScheduleBlockRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CleanCalendarCommandHandler implements MessageHandlerInterface
{
    /**
     * @var ScheduleBlockRepository
     */
    private ScheduleBlockRepository $scheduleBlockRepository;

    public function __construct(ScheduleBlockRepository $scheduleBlockRepository)
    {
        $this->scheduleBlockRepository = $scheduleBlockRepository;
    }

    public function __invoke(CleanCalendarCommand $command)
    {
        $schedules = $this->scheduleBlockRepository->findBy(['calendar' => $command->getCalendarId()]);

        foreach ($schedules as $schedule) {
            $this->scheduleBlockRepository->remove($schedule);
        }
    }

}