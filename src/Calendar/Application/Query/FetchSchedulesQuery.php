<?php


namespace App\Calendar\Application\Query;


use App\Account\Infrastructure\Messenger\ProfileMessage;
use App\Shared\Application\QueryInterface;

class FetchSchedulesQuery extends ProfileMessage implements QueryInterface
{
}