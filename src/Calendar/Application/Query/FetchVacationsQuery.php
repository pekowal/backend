<?php


namespace App\Calendar\Application\Query;


use App\Account\Infrastructure\Messenger\ProfileMessage;
use App\Shared\Application\QueryInterface;

class FetchVacationsQuery extends ProfileMessage implements QueryInterface
{
}