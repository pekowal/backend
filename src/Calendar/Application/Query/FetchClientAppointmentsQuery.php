<?php


namespace App\Calendar\Application\Query;


use App\Shared\Application\QueryInterface;

class FetchClientAppointmentsQuery implements QueryInterface
{
    private int $accountId;

    public function __construct(int $accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * @return int
     */
    public function getAccountId(): int
    {
        return $this->accountId;
    }
}