<?php


namespace App\Calendar\Application\AsyncCommandHandler;


use App\Calendar\Application\AsyncCommand\RecalculateScheduleBlocksCommand;
use App\Calendar\Application\Command\CleanCalendarCommand;
use App\Calendar\Infrastructure\Calculator\BlocksCalculator;
use App\Calendar\Infrastructure\Repository\CalendarRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class RecalculateScheduleBlockCommandHandler implements MessageHandlerInterface
{
    /**
     * @var CalendarRepository
     */
    private CalendarRepository $calendarRepository;

    /**
     * @var BlocksCalculator
     */
    private BlocksCalculator $blocksCalculator;
    /**
     * @var MessageBusInterface
     */
    private MessageBusInterface $commandBus;

    public function __construct(CalendarRepository $calendarRepository, BlocksCalculator $blocksCalculator, MessageBusInterface $commandBus)
    {
        $this->calendarRepository = $calendarRepository;
        $this->blocksCalculator = $blocksCalculator;
        $this->commandBus = $commandBus;
    }

    public function __invoke(RecalculateScheduleBlocksCommand $command)
    {
        $this->commandBus->dispatch(new CleanCalendarCommand($command->getCalendarId()));

        $this->blocksCalculator->refreshCalendar($command->getCalendarId());
    }
}