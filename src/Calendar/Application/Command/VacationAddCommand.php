<?php


namespace App\Calendar\Application\Command;


use App\Account\Infrastructure\Validator\Constraint\AccountRestricted;
use App\Calendar\Infrastructure\Validator\Constraint\Vacation;
use App\Shared\Application\QueryInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Vacation()
 */
final class VacationAddCommand implements QueryInterface
{
    /**
     * @AccountRestricted()
     * @Assert\NotBlank()
     * @Assert\Type(type="int")
     */
    private $profileId;

    /**
     * @Assert\NotBlank()
     * @Assert\DateTime(format="Y-m-d")
     */
    private $startDate;

    /**
     * @Assert\NotBlank()
     * @Assert\DateTime(format="Y-m-d")
     */
    private $endDate;

    /**
     * @Assert\Type(type="string")
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="30")
     */
    private $name;

    /**
     * @return mixed
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

    /**
     * @param mixed $profileId
     */
    public function setProfileId($profileId): void
    {
        $this->profileId = $profileId;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate): void
    {
        $this->endDate = $endDate;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }
}