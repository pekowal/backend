<?php


namespace App\Calendar\Application\Command;

use App\Shared\Application\CommandInterface;

final class AppointmentCancelCommand implements CommandInterface
{
    private int $appointmentId;
    private string $reason;

    public function __construct(int $appointmentId, string $reason)
    {
        $this->appointmentId = $appointmentId;
        $this->reason = $reason;
    }

    /**
     * @return int
     */
    public function getAppointmentId(): int
    {
        return $this->appointmentId;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

}