<?php


namespace App\Calendar\Application\Command;


use App\Account\Infrastructure\Messenger\ProfileMessage;

final class ScheduleRemoveCommand extends ProfileMessage
{
    private int $scheduleId;

    public function __construct($profileId, int $scheduleId)
    {
        parent::__construct($profileId);
        $this->scheduleId = $scheduleId;
    }


    /**
     * @return int
     */
    public function getScheduleId(): int
    {
        return $this->scheduleId;
    }
}