<?php


namespace App\Calendar\Application\Command;

use App\Account\Infrastructure\Validator\Constraint\AccountRestricted;

final class VacationRemoveCommand
{
    /**
     * @AccountRestricted()
     */
    private int $profileId;

    private int $vacationId;

    public function __construct(int $vacationId, int $profileId)
    {
        $this->vacationId = $vacationId;
        $this->profileId = $profileId;
    }

    /**
     * @return int
     */
    public function getProfileId(): int
    {
        return $this->profileId;
    }

    /**
     * @return int
     */
    public function getVacationId(): int
    {
        return $this->vacationId;
    }

}