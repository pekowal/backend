<?php


namespace App\Calendar\Application\Command;


use App\Shared\Application\AsyncCommandInterface;
use DateTime;

final class ScheduleBlockAddCommand implements AsyncCommandInterface
{
    private DateTime $startTime;
    private DateTime $endTime;
    private int $calendarId;

    public function __construct(DateTime $startTime, DateTime $endTime, int $calendarId)
  {
      $this->startTime = $startTime;
      $this->endTime = $endTime;
      $this->calendarId = $calendarId;
  }

    /**
     * @return DateTime
     */
    public function getStartTime(): DateTime
    {
        return $this->startTime;
    }

    /**
     * @return DateTime
     */
    public function getEndTime(): DateTime
    {
        return $this->endTime;
    }

    /**
     * @return int
     */
    public function getCalendarId(): int
    {
        return $this->calendarId;
    }

}