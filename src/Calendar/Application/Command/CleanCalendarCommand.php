<?php


namespace App\Calendar\Application\Command;


use App\Calendar\Infrastructure\Repository\ScheduleBlockRepository;

final class CleanCalendarCommand
{
    private int $calendarId;

    public function __construct(int $calendarId)
    {
        $this->calendarId = $calendarId;
    }

    /**
     * @return int
     */
    public function getCalendarId(): int
    {
        return $this->calendarId;
    }

}