<?php


namespace App\Calendar\Application\Command;

use App\Account\Infrastructure\Validator\Constraint\AccountRestricted;
use App\Calendar\Infrastructure\Validator\Constraint\DateMinutes;
use App\Calendar\Infrastructure\Validator\Constraint\Schedule;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Schedule()
 */
final class ScheduleAddCommand
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="int")
     * @Assert\Range(
     *      min = 0,
     *      max = 6,
     *      notInRangeMessage = "You must pass value between {{ min }} (Monday) and {{ max }}(Sunday)",
     * )
     */
    private $weekDay;

    /**
     * @AccountRestricted()
     */
    private $profileId;

    /**
     * @Assert\NotBlank()
     * @Assert\DateTime(format="H:i")
     * @DateMinutes()
     */
    private $startDate;

    /**
     * @Assert\NotBlank()
     * @Assert\DateTime(format="H:i")
     * @DateMinutes()
     */
    private $endDate;

    /**
     * @return mixed
     */
    public function getWeekDay()
    {
        return $this->weekDay;
    }

    /**
     * @param mixed $weekDay
     */
    public function setWeekDay($weekDay): void
    {
        $this->weekDay = $weekDay;
    }

    /**
     * @return mixed
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

    /**
     * @param mixed $profileId
     */
    public function setProfileId($profileId): void
    {
        $this->profileId = $profileId;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate): void
    {
        $this->endDate = $endDate;
    }

}