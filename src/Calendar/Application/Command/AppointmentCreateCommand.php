<?php


namespace App\Calendar\Application\Command;

use App\Shared\Application\CommandInterface;

final class AppointmentCreateCommand implements CommandInterface
{
    private int $expertProfileId;
    private int $clientAccountId;

    public function __construct(int $expertProfileId, int $clientAccountId)
    {
        $this->expertProfileId = $expertProfileId;
        $this->clientAccountId = $clientAccountId;
    }

    public function getExpertProfileId(): int
    {
        return $this->expertProfileId;
    }

    public function getClientAccountId(): int
    {
        return $this->clientAccountId;
    }
}