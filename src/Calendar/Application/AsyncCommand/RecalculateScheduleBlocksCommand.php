<?php


namespace App\Calendar\Application\AsyncCommand;


use App\Shared\Application\AsyncCommandInterface;

final class RecalculateScheduleBlocksCommand implements AsyncCommandInterface
{
    private int $calendarId;

    public function __construct(int $calendarId)
    {
        $this->calendarId = $calendarId;
    }

    /**
     * @return int
     */
    public function getCalendarId(): int
    {
        return $this->calendarId;
    }

}