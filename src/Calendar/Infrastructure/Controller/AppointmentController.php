<?php

namespace App\Calendar\Infrastructure\Controller;

use App\Calendar\Application\Query\FetchClientAppointmentsQuery;
use App\Calendar\Application\Query\FetchExpertAppointmentsQuery;
use App\Shared\Infrastructure\Controller\ApiAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/calendar")
 */
class AppointmentController extends ApiAbstractController
{
    /**
     * @IsGranted("ROLE_USER")
     *
     * @Route("/appointments/cancel/{appointmentId}", methods={"GET"}, name="get_client_appointments")
     *
     * @param int $appointmentId
     * @return JsonResponse
     */
    public function cancelAppointment(int $appointmentId): JsonResponse
    {
        $result = $this->queryBus->dispatch(new FetchClientAppointmentsQuery($this->getUser()->getId()));
        /** @var HandledStamp $handledStamp */
        $handledStamp = $result->last(HandledStamp::class);
        $appointments = $handledStamp->getResult();

        return $this->json($appointments, JsonResponse::HTTP_OK, [], ['groups' => 'frontend']);
    }

    /**
     * @IsGranted("ROLE_USER")
     *
     * @Route("/appointments/client", methods={"GET"}, name="get_client_appointments")
     *
     * @return JsonResponse
     */
    public function getClientAppointments(): JsonResponse
    {
        $result = $this->queryBus->dispatch(new FetchClientAppointmentsQuery($this->getUser()->getId()));
        /** @var HandledStamp $handledStamp */
        $handledStamp = $result->last(HandledStamp::class);
        $appointments = $handledStamp->getResult();

        return $this->json($appointments, JsonResponse::HTTP_OK, [], ['groups' => 'frontend']);
    }

    /**
     * @IsGranted("ROLE_EXPERT")
     *
     * @Route("/{profileId}/appointments", methods={"GET"}, name="get_appointments")
     *
     * @param int $profileId
     * @return JsonResponse
     */
    public function getAppointments(int $profileId): JsonResponse
    {
            $result = $this->queryBus->dispatch(new FetchExpertAppointmentsQuery($profileId));
            /** @var HandledStamp $handledStamp */
            $handledStamp = $result->last(HandledStamp::class);
            $appointments = $handledStamp->getResult();


        return $this->json($appointments, JsonResponse::HTTP_OK, [], ['groups' => 'frontend']);
    }

}