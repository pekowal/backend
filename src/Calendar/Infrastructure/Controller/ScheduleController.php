<?php


namespace App\Calendar\Infrastructure\Controller;


use App\Calendar\Application\Command\ScheduleAddCommand;
use App\Calendar\Application\Command\ScheduleRemoveCommand;
use App\Calendar\Application\Query\FetchSchedulesQuery;
use App\Calendar\Domain\Entity\Schedule;
use App\Calendar\Domain\Entity\Vacation;
use App\Calendar\Infrastructure\Controller\Dto\CalendarEventFactory;
use App\Shared\Infrastructure\Controller\ApiAbstractController;
use App\Shared\Infrastructure\ValidationErrorsFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/calendar")
 */
class ScheduleController extends ApiAbstractController
{

    /**
     * @IsGranted("ROLE_EXPERT")
     *
     * @Route("/{profileId}/schedules", methods={"GET"}, name="get_schedules")
     */
    public function getSchedules(int $profileId, CalendarEventFactory $calendarEventFactory): JsonResponse
    {
        $result = $this->queryBus->dispatch(new FetchSchedulesQuery($profileId));
        /** @var HandledStamp $handledStamp */
        $handledStamp = $result->last(HandledStamp::class);
        $schedules = $handledStamp->getResult();

        return $this->json($calendarEventFactory->createCalendarEvents($schedules));
    }

    /**
     * @IsGranted("ROLE_EXPERT")
     *
     * @Route("/{profileId}/schedules/add", methods={"POST"}, name="schedule_add")
     */
    public function addSchedule(Request $request, CalendarEventFactory $calendarEventFactory): JsonResponse
    {
        $scheduleAddCommand = $this->serializer->deserialize($request->getContent(), ScheduleAddCommand::class, 'json');

        try {
            $envelope = $this->messageBus->dispatch($scheduleAddCommand);
            /** @var HandledStamp $handledStamp */
            $handledStamp = $envelope->last(HandledStamp::class);
            /** @var Schedule $schedule */
            $schedule = $handledStamp->getResult();
        } catch (ValidationFailedException $validationFailedException) {
            $errorsMessages = (new ValidationErrorsFetcher($validationFailedException->getViolations()))->getErrorsMessages();

            return new JsonResponse(['errors' => $errorsMessages], JsonResponse::HTTP_BAD_REQUEST);
        }

        return $this->json($calendarEventFactory->createCalendarEventDto($schedule));
    }

    /**
     * @IsGranted("ROLE_EXPERT")
     *
     * @Route("/{profileId}/schedules/{scheduleId}/delete", methods={"DELETE"}, name="schedule_delete")
     */
    public function removeSchedule(int $scheduleId, int $profileId): JsonResponse
    {
        $command = new ScheduleRemoveCommand($profileId, $scheduleId);
        $this->messageBus->dispatch($command);

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }
}