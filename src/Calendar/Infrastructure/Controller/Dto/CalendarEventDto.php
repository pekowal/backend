<?php


namespace App\Calendar\Infrastructure\Controller\Dto;


final class CalendarEventDto
{
    private int $id;
    private string $type;
    private string $name;
    private string $startDate;
    private string $endDate;

    public function __construct(int $id, string $type, string $name, string $startDate, string $endDate)
    {
        $this->id = $id;
        $this->type = $type;
        $this->name = $name;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getStartDate(): string
    {
        return $this->startDate;
    }

    /**
     * @return string
     */
    public function getEndDate(): string
    {
        return $this->endDate;
    }

}