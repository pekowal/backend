<?php


namespace App\Calendar\Infrastructure\Controller\Dto;


use App\Calendar\Infrastructure\CalendarEventInterface;

class CalendarEventFactory
{
    /**
     * @param CalendarEventInterface[] $calendarEvents
     *
     * @return CalendarEventDto[]
     */
    public function createCalendarEvents(array $calendarEvents): array
    {
        $events = [];

        foreach ($calendarEvents as $calendarEvent) {
            $events[] = $this->createCalendarEventDto($calendarEvent);
        }

        return $events;
    }

    public function createCalendarEventDto(CalendarEventInterface $calendarEvent): CalendarEventDto
    {
        return new CalendarEventDto(
            $calendarEvent->getId(),
            $calendarEvent->getType(),
            $calendarEvent->getName(),
            $calendarEvent->getStartDate()->format('Y-m-d H:i'),
            $calendarEvent->getEndDate()->format('Y-m-d H:i')
        );
    }
}