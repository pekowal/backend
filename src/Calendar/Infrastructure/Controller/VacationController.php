<?php


namespace App\Calendar\Infrastructure\Controller;

use App\Calendar\Application\Command\VacationAddCommand;
use App\Calendar\Application\Command\VacationRemoveCommand;
use App\Calendar\Application\Query\FetchVacationsQuery;
use App\Calendar\Domain\Entity\Vacation;
use App\Shared\Infrastructure\Controller\ApiAbstractController;
use App\Shared\Infrastructure\ValidationErrorsFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/calendar")
 */
final class VacationController extends ApiAbstractController
{
    /**
     * @IsGranted("ROLE_EXPERT")
     *
     * @Route("/vacation/add", methods={"POST"}, name="vacation_add")
     */
    public function addVacation(Request $request): JsonResponse
    {
        $vacationAddCommand = $this->serializer->deserialize($request->getContent(), VacationAddCommand::class, 'json');

        try {
            $result = $this->messageBus->dispatch($vacationAddCommand);
            /** @var HandledStamp $handledStamp */
            $handledStamp = $result->last(HandledStamp::class);
            /** @var Vacation $vacation */
            $vacation = $handledStamp->getResult();
        } catch (ValidationFailedException $validationFailedException) {
            $errorsMessages = (new ValidationErrorsFetcher($validationFailedException->getViolations()))->getErrorsMessages();

            return new JsonResponse(['errors' => $errorsMessages], JsonResponse::HTTP_BAD_REQUEST);
        }

        return new JsonResponse($vacation->getId());
    }

    /**
     * @IsGranted("ROLE_EXPERT")
     *
     * @Route("/vacations/{vacationId}/edit", methods={"POST"}, name="vacation_edit")
     */
    public function editVacation(Request $request): JsonResponse
    {
        //TODO
        return new JsonResponse();
    }

    /**
     * @IsGranted("ROLE_EXPERT")
     *
     * @Route("/{profileId}/vacations", methods={"GET"}, name="get_vacations")
     */
    public function getVacations(int $profileId): JsonResponse
    {
        $result = $this->queryBus->dispatch(new FetchVacationsQuery($profileId));
        /** @var HandledStamp $handledStamp */
        $handledStamp = $result->last(HandledStamp::class);
        $vacations = $handledStamp->getResult();

        return $this->json($vacations, JsonResponse::HTTP_OK, [], ['groups' => 'frontend']);
    }

    /**
     * @IsGranted("ROLE_EXPERT")
     *
     * @Route("/{profileId}/vacations/{id}/delete", methods={"DELETE"}, name="vacation_delete")
     */
    public function deleteVacation(int $id, int $profileId): JsonResponse
    {
        $vacationRemoveCommand = new VacationRemoveCommand($id, $profileId);
        $this->messageBus->dispatch($vacationRemoveCommand);

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }
}