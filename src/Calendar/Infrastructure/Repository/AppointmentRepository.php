<?php


namespace App\Calendar\Infrastructure\Repository;

use App\Calendar\Domain\Entity\Appointment;
use App\Shared\Infrastructure\AbstractEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

class AppointmentRepository extends AbstractEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, Appointment::class, $entityManager);
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $profileId
     * @return Appointment[]
     */
    public function fetchAppointments(int $profileId): array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.expertProfile = :profileId')
            ->setParameter('profileId', $profileId)->getQuery()->getResult();
    }
}