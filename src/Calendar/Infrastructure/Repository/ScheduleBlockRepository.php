<?php


namespace App\Calendar\Infrastructure\Repository;

use App\Calendar\Domain\Entity\ScheduleBlock;
use App\Shared\Infrastructure\AbstractEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

class ScheduleBlockRepository extends AbstractEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, ScheduleBlock::class, $entityManager);
        $this->entityManager = $entityManager;
    }
}