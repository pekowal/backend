<?php


namespace App\Calendar\Infrastructure\Repository;

use App\Calendar\Application\Command\ScheduleAddCommand;
use App\Calendar\Domain\Entity\Appointment;
use App\Calendar\Domain\Entity\Schedule;
use App\Calendar\Infrastructure\Utils\ScheduleDayHelper;
use App\Shared\Infrastructure\AbstractEntityRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

class ScheduleRepository extends AbstractEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, Schedule::class, $entityManager);
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $profileId
     * @return Schedule[]
     */
    public function fetchSchedules(int $profileId): array
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.calendar', 'c')
            ->andWhere('c.profile = :profileId')
            ->setParameter('profileId', $profileId)->getQuery()->getResult();
    }

    /**
     * @param ScheduleAddCommand $scheduleAddCommand
     *
     * @return Schedule|null
     *
     * @throws NonUniqueResultException
     */
    public function fetchSchedule(ScheduleAddCommand $scheduleAddCommand): ?Schedule
    {
        $startHour = ScheduleDayHelper::convertTimeToHours($scheduleAddCommand->getStartDate());
        $endHour = ScheduleDayHelper::convertTimeToHours($scheduleAddCommand->getEndDate());

        return $this->createQueryBuilder('s')
            ->leftJoin('s.calendar', 'c')
            ->andWhere('c.profile = :profileId')
            ->andWhere('s.weekDay = :weekDay')
            ->andWhere('s.startHour <= :startHour AND s.endHour >= :endHour')
            ->orWhere('s.startHour > :startHour AND s.startHour < :endHour')
            ->orWhere('s.endHour > :startHour AND s.endHour < :endHour')
            ->setParameter('startHour', $startHour)
            ->setParameter('endHour', $endHour)
            ->setParameter('profileId', $scheduleAddCommand->getProfileId())
            ->setParameter('weekDay', $scheduleAddCommand->getWeekDay())
            ->getQuery()->getOneOrNullResult();
    }

}
