<?php


namespace App\Calendar\Infrastructure\Repository;

use App\Calendar\Domain\Entity\Vacation;
use App\Shared\Infrastructure\AbstractEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

class VacationRepository extends AbstractEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, Vacation::class, $entityManager);
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $profileId
     *
     * @return Vacation[]
     */
    public function fetchVacations(int $profileId): array
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.profile = :profileId')
            ->setParameter('profileId', $profileId)
            ->getQuery()->getResult();
    }
}