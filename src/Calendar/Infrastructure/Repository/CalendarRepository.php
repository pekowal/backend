<?php


namespace App\Calendar\Infrastructure\Repository;

use App\Calendar\Domain\Entity\Calendar;
use App\Shared\Infrastructure\AbstractEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

class CalendarRepository extends AbstractEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, Calendar::class, $entityManager);
        $this->entityManager = $entityManager;
    }
}