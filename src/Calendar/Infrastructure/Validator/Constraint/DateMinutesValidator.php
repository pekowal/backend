<?php


namespace App\Calendar\Infrastructure\Validator\Constraint;

use DateTime;
use Exception;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

final class DateMinutesValidator extends ConstraintValidator
{
    public function __construct()
    {
    }

    public function validate($value, Constraint $constraint)
    {
        /** @var DateMinutes $constraint */
        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        try {
            $date = new DateTime($value);
        } catch (Exception $e) {
            return;
        }

        $minutes = (int)$date->format('i');

        if ($minutes === 0 || $minutes === 30) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->setParameter('{{ string }}', $value)
            ->addViolation();
    }
}