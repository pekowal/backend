<?php


namespace App\Calendar\Infrastructure\Validator\Constraint;


use App\Calendar\Application\Command\VacationAddCommand;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

class VacationValidator extends ConstraintValidator
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param VacationAddCommand $schedule
     * @param Constraint $constraint
     */
    public function validate($schedule, Constraint $constraint)
    {
        if ($schedule->getEndDate() < $schedule->getStartDate()) {
            $message = $this->translator->trans($constraint->message);
            $this->context->buildViolation($message)
                ->atPath('endDate')
                ->addViolation();
        }
    }
}