<?php


namespace App\Calendar\Infrastructure\Validator\Constraint;


use App\Account\Domain\Entity\Profile;
use App\Account\Infrastructure\Repository\ProfileRepository;
use App\Calendar\Application\Command\ScheduleAddCommand;
use App\Calendar\Infrastructure\Repository\ScheduleRepository;
use App\Calendar\Infrastructure\Utils\ScheduleDayHelper;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

class ScheduleValidator extends ConstraintValidator
{
    private TranslatorInterface $translator;
    private ScheduleRepository $scheduleRepository;
    private ProfileRepository $profileRepository;

    public function __construct(TranslatorInterface $translator, ScheduleRepository $scheduleRepository, ProfileRepository $profileRepository)
    {
        $this->translator = $translator;
        $this->scheduleRepository = $scheduleRepository;
        $this->profileRepository = $profileRepository;
    }

    /**
     * @param ScheduleAddCommand $schedule
     * @param Constraint $constraint
     *
     * @throws NonUniqueResultException
     */
    public function validate($schedule, Constraint $constraint)
    {
        if ($schedule->getEndDate() < $schedule->getStartDate()) {
            $message = $this->translator->trans($constraint->message);
            $this->context->buildViolation($message)
                ->atPath('endDate')
                ->addViolation();
        }

        $schedule = $this->scheduleRepository->fetchSchedule($schedule);

        if ($schedule) {
            $this->context->buildViolation('Dostępność nachodzi na siebie')
                ->atPath('endDate')
                ->addViolation();
        }

    }
}