<?php


namespace App\Calendar\Infrastructure\Validator\Constraint;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Schedule extends Constraint
{
    public $message = 'End time should be after start time.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}