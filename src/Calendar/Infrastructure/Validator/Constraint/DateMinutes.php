<?php


namespace App\Calendar\Infrastructure\Validator\Constraint;



use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
final class DateMinutes extends Constraint
{
    public $message = 'This value is not a valid time.';

}