<?php


namespace App\Calendar\Infrastructure\Utils;

use DateTime;

class ScheduleDayHelper
{
    const WEEK_DAYS = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

    public static function getCorrectDate(int $weekDay, float $hours): DateTime
    {
        $date = new DateTime();
        $date->setTimestamp(strtotime(self::WEEK_DAYS[$weekDay]. ' this week'));
        $hour = (int)floor($hours);
        $minutes = ($hours - $hour) * 60;

        $date->setTime($hour, (int)$minutes);

        return $date;
    }

    public static function convertTimeToHours(string $time): float
    {
        $time = explode(':', $time);
        $hours = (float)$time[0];
        $minutes = (float)$time[1];

        return $hours + ($minutes / 60);
    }

}