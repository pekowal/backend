<?php


namespace App\Calendar\Infrastructure\Calculator;


use App\Calendar\Infrastructure\Repository\ScheduleRepository;

class BlocksCalculator
{
    /**
     * @var ScheduleRepository
     */
    private ScheduleRepository $scheduleRepository;

    public function __construct(ScheduleRepository $scheduleRepository)
    {
        $this->scheduleRepository = $scheduleRepository;
    }

    public function refreshCalendar(int $calendarId)
    {
        $schedules = $this->scheduleRepository->findBy(['calendar' => $calendarId]);


    }
}