<?php


namespace App\Calendar\Infrastructure;


use DateTime;

interface CalendarEventInterface
{
    public function getId(): ?int;
    public function getName(): string;
    public function getStartDate(): DateTime;
    public function getEndDate(): DateTime;
    public function getType(): string;
}