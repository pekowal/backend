<?php

namespace App\Calendar\Domain\Entity;

use App\Calendar\Infrastructure\CalendarEventInterface;
use App\Calendar\Infrastructure\Utils\ScheduleDayHelper;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Day schedule for work
 *
 * @ORM\Entity(repositoryClass="App\Calendar\Infrastructure\Repository\ScheduleRepository")
 */
class Schedule implements CalendarEventInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * @ORM\Column(type="integer")
     */
    private int $weekDay;

    /**
     * @ORM\Column(type="float")
     */
    private float $startHour;

    /**
     * @ORM\Column(type="float")
     */
    private float $endHour;

    /**
     * @ORM\ManyToOne(targetEntity="Calendar")
     * @ORM\JoinColumn(name="calendar_id", referencedColumnName="id")
     */
    private Calendar $calendar;

    public function __construct(Calendar $calendar, int $weekDay, string $name, float $startHour, float $endHour)
    {
        $this->calendar = $calendar;
        $this->weekDay = $weekDay;
        $this->startHour = $startHour;
        $this->endHour = $endHour;
        $this->name = $name;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Calendar
     */
    public function getCalendar(): Calendar
    {
        return $this->calendar;
    }

    /**
     * @return int
     */
    public function getWeekDay(): int
    {
        return $this->weekDay;
    }

    /**
     * @return DateTime
     */
    public function getStartDate(): DateTime
    {
        return ScheduleDayHelper::getCorrectDate($this->weekDay, $this->startHour);
    }

    /**
     * @return DateTime
     */
    public function getEndDate(): DateTime
    {
        return ScheduleDayHelper::getCorrectDate($this->weekDay, $this->endHour);
    }


    public function getType(): string
    {
        return 'schedule';
    }
}