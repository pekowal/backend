<?php


namespace App\Calendar\Domain\Entity;

use App\Account\Domain\Entity\Account;
use App\Account\Domain\Entity\Profile;
use App\Calendar\Infrastructure\CalendarEventInterface;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity()
 */
class Appointment implements CalendarEventInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\OneToMany(targetEntity="ScheduleBlock", mappedBy="appointment")
     */
    private Collection $scheduleBlocks;

    /**
     * @ORM\ManyToOne(targetEntity="App\Account\Domain\Entity\Profile")
     * @ORM\JoinColumn(name="expert_profile_id", referencedColumnName="id")
     */
    private Profile $expertProfile;

    /**
     * Aka client
     *
     * @ORM\ManyToOne(targetEntity="App\Account\Domain\Entity\Account")
     * @ORM\JoinColumn(name="client_account_id", referencedColumnName="id")
     */
    private Account $clientAccount;

    /**
     * Appointment constructor.
     * @param Profile $expertProfile
     * @param Account $clientAccount
     * @param ScheduleBlock[] $scheduleBlocks
     */
    public function __construct(Profile $expertProfile, Account $clientAccount, array $scheduleBlocks)
    {
        $this->scheduleBlocks = new ArrayCollection($scheduleBlocks);
        $this->expertProfile = $expertProfile;
        $this->clientAccount = $clientAccount;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getScheduleBlocks(): Collection
    {
        return $this->scheduleBlocks;
    }

    public function addScheduleBlock(ScheduleBlock $scheduleBlock): void
    {
        $this->scheduleBlocks->add($scheduleBlock);
    }

    public function getStartDate(): DateTime
    {
        /** @var ScheduleBlock[] $scheduleBlocks */
        $scheduleBlocks = $this->getScheduleBlocks()->toArray();
        usort($scheduleBlocks, fn (ScheduleBlock $a , ScheduleBlock $b) => $a->getStartDate() < $b->getStartDate());

        return $scheduleBlocks[0]->getStartDate();
    }

    public function getEndDate(): DateTime
    {
        /** @var ScheduleBlock[] $scheduleBlocks */
        $scheduleBlocks = $this->getScheduleBlocks()->toArray();
        usort($scheduleBlocks, fn (ScheduleBlock $a , ScheduleBlock $b) => $a->getEndDate() > $b->getEndDate());

        return $scheduleBlocks[0]->getEndDate();
    }

    public function getExpertProfile(): Profile
    {
        return $this->expertProfile;
    }


    public function getClientAccount(): Account
    {
        return $this->clientAccount;
    }

    public function getName(): string
    {
        return 'Spotkanie z '. $this->clientAccount->getUsername();
    }

    public function getType(): string
    {
        return 'appointment';
    }
}