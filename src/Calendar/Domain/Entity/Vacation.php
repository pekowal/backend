<?php


namespace App\Calendar\Domain\Entity;

use App\Account\Domain\Entity\Profile;
use App\Calendar\Infrastructure\CalendarEventInterface;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Free of work
 *
 * @ORM\Entity()
 */
class Vacation implements CalendarEventInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Account\Domain\Entity\Profile")
     * @ORM\JoinColumn(name="profileId", referencedColumnName="id")
     */
    private Profile $profile;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTime $startDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTime $endDate;

    /**
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * Schedule constructor.
     *
     * @param Profile $profile
     * @param DateTime $startTime
     * @param DateTime $endTime
     * @param string $name
     */
    public function __construct(Profile $profile, DateTime $startTime, DateTime $endTime, string $name)
    {
        $this->profile = $profile;
        $this->startDate = $startTime;
        $this->endDate = $endTime;
        $this->name = $name;
    }

    public function getProfile(): Profile
    {
        return $this->profile;
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return DateTime
     */
    public function getStartDate(): DateTime
    {
        return $this->startDate;
    }

    /**
     * @return DateTime
     */
    public function getEndDate(): DateTime
    {
        return $this->endDate;
    }

    public function getType(): string
    {
        return 'vacation';
    }

}
