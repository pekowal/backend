<?php


namespace App\Calendar\Domain\Entity;

use App\Calendar\Infrastructure\CalendarEventInterface;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Work block schedule
 *
 * @ORM\Entity()
 */
class ScheduleBlock implements CalendarEventInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTime $startDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTime $endDate;

    /**
     * @ORM\ManyToOne(targetEntity="Calendar")
     * @ORM\JoinColumn(name="calendar_id", referencedColumnName="id")
     */
    private Calendar $calendar;

    /**
     * @ORM\ManyToOne(targetEntity="Appointment")
     * @ORM\JoinColumn(name="appointment_id", referencedColumnName="id")
     */
    private ?Appointment $appointment = null;

    public function __construct(DateTime $startDate, DateTime $endDate, Calendar $calendar)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->calendar = $calendar;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Calendar
     */
    public function getCalendar(): Calendar
    {
        return $this->calendar;
    }

    /**
     * @param Calendar $calendar
     */
    public function setCalendar(Calendar $calendar): void
    {
        $this->calendar = $calendar;
    }

    /**
     * @return Appointment|null
     */
    public function getAppointment(): ?Appointment
    {
        return $this->appointment;
    }

    /**
     * @param Appointment|null $appointment
     */
    public function setAppointment(?Appointment $appointment): void
    {
        $this->appointment = $appointment;
    }

    /**
     * @return DateTime
     */
    public function getStartDate(): DateTime
    {
        return $this->startDate;
    }

    /**
     * @param DateTime $startDate
     */
    public function setStartDate(DateTime $startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @return DateTime
     */
    public function getEndDate(): DateTime
    {
        return $this->endDate;
    }

    /**
     * @param DateTime $endDate
     */
    public function setEndDate(DateTime $endDate): void
    {
        $this->endDate = $endDate;
    }


    public function getName(): string
    {
        return 'Dostępność';
    }

    public function getType(): string
    {
        return 'schedule_block';
    }
}