<?php


namespace App\Account\Application\Command;

use App\Shared\Application\AsyncCommandInterface;
use App\Shared\Application\Validator\Constraints\EntityExist;
use Symfony\Component\Validator\Constraints as Assert;

class CreateAccountCommand implements AsyncCommandInterface
{
    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Assert\Length(min=3)
     * @EntityExist(className="App\Account\Domain\Entity\Account", fieldName="email")
     */
    private string $email;

    /**
     * @todo add validator for password
     *
     * @Assert\NotBlank()
     * @Assert\Length(min=6)
     */
    private string $plainPassword;

    public function __construct(string $email, string $plainPassword)
    {
        $this->email = $email;
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPlainPassword(): string
    {
        return $this->plainPassword;
    }

}