<?php


namespace App\Account\Application\Command;

/**
 * Creates expert account with profile and calendar
 */
final class CreateExpertAccountCommand extends CreateAccountCommand
{
    private string $firstName;
    private string $lastName;

    public function __construct(string $email, string $plainPassword, string $firstName, string $lastName)
    {
        parent::__construct($email, $plainPassword);
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }


    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }
    
}