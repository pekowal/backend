<?php


namespace App\Account\Application\Command;

use App\Account\Infrastructure\Validator\Constraint\AccountRestricted;
use Symfony\Component\Validator\Constraints as Assert;
use App\Shared\Application\QueryInterface;

final class AddServiceCommand implements QueryInterface
{
    /**
     * @AccountRestricted()
     * @Assert\NotBlank()
     * @Assert\Type(type="int")
     */
    private $profileId;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     */
    private $name;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="int")
     */
    private $time;

    /**
     * @return mixed
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

    /**
     * @param mixed $profileId
     */
    public function setProfileId($profileId): void
    {
        $this->profileId = $profileId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time): void
    {
        $this->time = $time;
    }
}