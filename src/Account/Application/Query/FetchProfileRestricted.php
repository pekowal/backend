<?php


namespace App\Account\Application\Query;


use App\Shared\Application\Validator\Constraints\EntityExist;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Fetch profile for logged user
 */
final class FetchProfileRestricted
{
    /**
     * @EntityExist()
     * @Assert\NotBlank()
     */
    private int $profileId;

    public function __construct(int $profileId)
    {
        $this->profileId = $profileId;
    }

    /**
     * @return int
     */
    public function getProfileId(): int
    {
        return $this->profileId;
    }

}