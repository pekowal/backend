<?php


namespace App\Account\Application\Console;


use App\Account\Application\Command\CreateAccountCommand;
use App\Account\Application\Command\CreateExpertAccountCommand;
use App\Account\Domain\Entity\Account;
use App\Account\Infrastructure\Repository\AccountRepository;
use App\Shared\Infrastructure\ValidationErrorsFetcher;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\Messenger\MessageBusInterface;

class CreateAdminCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-admin';

    /**
     * @var MessageBusInterface
     */
    private MessageBusInterface $messageBus;
    /**
     * @var AccountRepository
     */
    private AccountRepository $accountRepository;

    public function __construct(string $name = null, MessageBusInterface $messageBus, AccountRepository $accountRepository)
    {
        parent::__construct($name);
        $this->messageBus = $messageBus;
        $this->accountRepository = $accountRepository;
    }

    protected function configure()
    {
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'What is your email?')
            ->addArgument('password', InputArgument::REQUIRED, 'Please input password for account')
            ->setDescription('Creates a new admin.')
            ->setHelp('This command allows you to create a admin')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getArgument('email');
        $password = $input->getArgument('password');


        try {
            $this->messageBus->dispatch(new CreateExpertAccountCommand($email, $password));
        } catch (ValidationFailedException $exception) {

            $errors = (new ValidationErrorsFetcher($exception->getViolations()))->getErrorsMessages();

            foreach ($errors as $key => $val) {
                $errorTag = sprintf('<error>%s: %s</error>', $key, $val);
                $output->writeln($errorTag);
            }

            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}