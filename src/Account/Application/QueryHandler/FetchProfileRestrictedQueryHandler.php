<?php


namespace App\Account\Application\QueryHandler;


use App\Account\Application\Query\FetchProfileRestricted;
use App\Account\Infrastructure\Repository\ProfileRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Security\Core\Security;

final class FetchProfileRestrictedQueryHandler implements MessageHandlerInterface
{
    /**
     * @var Security
     */
    private Security $security;

    public function __construct(Security $security, ProfileRepository $profileRepository)
    {
        $this->security = $security;
    }

    /**
     * @param FetchProfileRestricted $fetchProfileRestricted
     */
    public function __invoke(FetchProfileRestricted $fetchProfileRestricted): void
    {

        $fetchProfileRestricted->getProfileToken();

        return;
    }
}