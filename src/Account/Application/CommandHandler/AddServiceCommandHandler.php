<?php


namespace App\Account\Application\CommandHandler;


use App\Account\Application\Command\AddServiceCommand;
use App\Account\Domain\Entity\Service;
use App\Account\Infrastructure\Repository\ProfileRepository;
use App\Account\Infrastructure\Repository\ServiceRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class AddServiceCommandHandler implements MessageHandlerInterface
{
    /**
     * @var ServiceRepository
     */
    private ServiceRepository $serviceRepository;

    /**
     * @var ProfileRepository
     */
    private ProfileRepository $profileRepository;

    public function __construct(ServiceRepository $serviceRepository, ProfileRepository $profileRepository)
    {
        $this->serviceRepository = $serviceRepository;
        $this->profileRepository = $profileRepository;
    }

    /**
     * @param AddServiceCommand $addServiceCommand
     */
    public function __invoke(AddServiceCommand $addServiceCommand): void
    {
        $service = new Service(
            $addServiceCommand->getName(),
            $addServiceCommand->getTime(),
            $this->profileRepository->find($addServiceCommand->getProfileId())
        );
        $this->serviceRepository->persist($service);
    }
}