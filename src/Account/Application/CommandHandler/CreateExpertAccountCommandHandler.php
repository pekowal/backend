<?php


namespace App\Account\Application\CommandHandler;

use App\Account\Application\Command\CreateExpertAccountCommand;
use App\Account\Domain\Entity\Personality;
use App\Account\Domain\Entity\Profile;
use App\Account\Domain\Factory\CreateAccountFactory;
use App\Account\Infrastructure\Repository\AccountRepository;
use App\Account\Infrastructure\Repository\ProfileRepository;
use App\Calendar\Domain\Entity\Calendar;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateExpertAccountCommandHandler implements MessageHandlerInterface
{
    /**
     * @var ProfileRepository
     */
    private ProfileRepository $profileRepository;

    /**
     * @var AccountRepository
     */
    private AccountRepository $accountRepository;
    /**
     * @var CreateAccountFactory
     */
    private CreateAccountFactory $createAccountFactory;

    public function __construct(ProfileRepository $profileRepository, AccountRepository $accountRepository, CreateAccountFactory $createAccountFactory)
    {
        $this->profileRepository = $profileRepository;
        $this->accountRepository = $accountRepository;
        $this->createAccountFactory = $createAccountFactory;
    }

    public function __invoke(CreateExpertAccountCommand $createExpertAccountCommand)
    {
        $account = $this->createAccountFactory->createAccount($createExpertAccountCommand);
        $account->grantExpertRole();

        $personality = new Personality($createExpertAccountCommand->getFirstName(), $createExpertAccountCommand->getLastName());
        $profile = new Profile($account, $personality, new Calendar());

        $this->profileRepository->persist($profile);
    }
}