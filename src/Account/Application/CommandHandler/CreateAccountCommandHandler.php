<?php


namespace App\Account\Application\CommandHandler;

use App\Account\Application\Command\CreateAccountCommand;
use App\Account\Domain\Factory\CreateAccountFactory;
use App\Account\Infrastructure\Repository\AccountRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateAccountCommandHandler implements MessageHandlerInterface
{
    private AccountRepository $accountRepository;


    private CreateAccountFactory $createAccountFactory;

    public function __construct(AccountRepository $accountRepository, CreateAccountFactory $createAccountFactory)
    {
        $this->accountRepository = $accountRepository;
        $this->createAccountFactory = $createAccountFactory;
    }

    public function __invoke(CreateAccountCommand $createAccountCommand)
    {
        $accountEntity = $this->createAccountFactory->createAccount($createAccountCommand);
        $this->accountRepository->persist($accountEntity);
    }
}