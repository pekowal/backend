<?php


namespace App\Account\Application\CommandHandler;

use App\Account\Application\Command\CreateAccountCommand;
use App\Account\Domain\Entity\Account;
use App\Account\Domain\Factory\CreateAccountFactory;
use App\Account\Infrastructure\Repository\AccountRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateAdminCommandHandler implements MessageHandlerInterface
{
    private AccountRepository $accountRepository;

    private CreateAccountFactory $createAccountFactory;

    public function __construct(AccountRepository $accountRepository, CreateAccountFactory $createAccountFactory)
    {
        $this->accountRepository = $accountRepository;
        $this->createAccountFactory = $createAccountFactory;
    }

    public function __invoke(CreateAccountCommand $createAccountCommand)
    {
        $accountEntity = $this->createAccountFactory->createAccount($createAccountCommand);
        $accountEntity->grantAdminRole();

        $this->accountRepository->persist($accountEntity);
    }
}