<?php


namespace App\Account\Infrastructure\Repository;

use App\Account\Domain\Entity\Account;
use App\Account\Domain\Entity\Profile;
use App\Shared\Infrastructure\AbstractEntityRepository;
use App\Shared\Infrastructure\EntityRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

final class ProfileRepository extends AbstractEntityRepository implements EntityRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, Profile::class, $entityManager);
        $this->entityManager = $entityManager;
    }

    public function fetchAccountProfile(int $accountId, int $profileId): ?Profile
    {
        return $this->createQueryBuilder('p')
            ->leftJoin('p.account', 'a')
            ->andWhere('a.id = :accountId')
            ->setParameter('accountId', $accountId)
            ->andWhere('p.id = :profileId')
            ->setParameter('profileId', $profileId)
            ->getQuery()->getOneOrNullResult();
    }
}