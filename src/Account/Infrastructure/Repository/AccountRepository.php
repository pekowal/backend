<?php


namespace App\Account\Infrastructure\Repository;

use App\Account\Domain\Entity\Account;
use App\Shared\Infrastructure\AbstractEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

final class AccountRepository extends AbstractEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, Account::class, $entityManager);
        $this->entityManager = $entityManager;
    }

    /**
     * @param null $lockMode
     * @param null $lockVersion
     *
     * @return Account
     *
     * @throws EntityNotFoundException
     */
    public function find($id, $lockMode = null, $lockVersion = null): Account
    {
        /** @var Account $account */
        $account = parent::find($id, $lockMode, $lockVersion);

        if (null === $account) {
            throw new EntityNotFoundException(sprintf('Account Entity with id "%s" not found', $id));
        }

        return $account;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneByApiToken(string $apiToken): ?Account
    {
        return $this->createQueryBuilder('a')
            ->leftJoin('a.token', 't')
            ->andWhere('t.token = :token')
            ->setParameter('token', $apiToken)->getQuery()->getOneOrNullResult();
    }

    public function exists(Account $account): bool
    {
        return (bool)$this->findOneBy(['email' => $account->getEmail()]);
    }
}