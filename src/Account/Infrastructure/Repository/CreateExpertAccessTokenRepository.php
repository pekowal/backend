<?php


namespace App\Account\Infrastructure\Repository;


use App\Account\Domain\Entity\CreateExpertAccessToken;
use App\Shared\Infrastructure\AbstractEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

final class CreateExpertAccessTokenRepository extends AbstractEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, CreateExpertAccessToken::class, $entityManager);
        $this->entityManager = $entityManager;
    }
}