<?php


namespace App\Account\Infrastructure\Messenger;


use App\Account\Infrastructure\Validator\Constraint\AccountRestricted;
use App\Account\Infrastructure\Validator\Constraint\ProfileRestricted;
use Symfony\Component\Validator\Constraints as Assert;

abstract class ProfileMessage
{
    /**
     * @AccountRestricted()
     * @Assert\Type(type="int")
     * @Assert\NotBlank()
     */
    private $profileId;

    public function __construct($profileId)
    {
        $this->profileId = $profileId;
    }

    /**
     * @return mixed
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

}