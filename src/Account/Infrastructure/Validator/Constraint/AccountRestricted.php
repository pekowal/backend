<?php

namespace App\Account\Infrastructure\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
final class AccountRestricted extends Constraint
{
    public $message = 'This value is already used.';
}
