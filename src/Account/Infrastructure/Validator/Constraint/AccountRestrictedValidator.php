<?php


namespace App\Account\Infrastructure\Validator\Constraint;


use App\Account\Infrastructure\Repository\ProfileRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

final class AccountRestrictedValidator extends ConstraintValidator
{
    /**
     * @var ProfileRepository
     */
    private ProfileRepository $profileRepository;

    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage, ProfileRepository $profileRepository)
    {
        $this->profileRepository = $profileRepository;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var AccountRestricted $constraint */
        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }
        if (!$token = $this->tokenStorage->getToken()) {
            return;
        }

        if (!$token->getUser()) {
            return;
        }

       ;
        if (null === $this->tokenStorage->getToken()) {
            return;
        }

        $profile = $this->profileRepository->fetchAccountProfile($token->getUser()->getId(), $value);

        if (null === $profile) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }
}