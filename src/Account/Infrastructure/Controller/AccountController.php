<?php


namespace App\Account\Infrastructure\Controller;

use App\Account\Domain\Entity\Account;
use App\Account\Domain\Entity\Profile;
use App\Account\Infrastructure\Controller\Dto\Profile as ProfileDto;
use App\Account\Infrastructure\Controller\Dto\User;
use App\Account\Infrastructure\Repository\ProfileRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

final class AccountController extends AbstractController
{
    /**
     * @var ProfileRepository
     */
    private ProfileRepository $profileRepository;

    public function __construct(ProfileRepository $profileRepository)
    {
        $this->profileRepository = $profileRepository;
    }

    /**
     * @IsGranted("ROLE_USER")
     *
     * @Route("/account", methods={"GET"}, name="fetch_account")
     */
    public function getAccount(Request $request): JsonResponse
    {
        /** @var Account $user */
        $user = $this->getUser();
        //TODO fetch better bc there are can be many profiles
        /** @var Profile $profile */
        $profile = $this->profileRepository->findOneBy(['account' => $user]);

        $frontendUser = new User($user->getEmail(), $user->getRoles(), new ProfileDto($profile->getId()));

        return $this->json($frontendUser);
    }

}