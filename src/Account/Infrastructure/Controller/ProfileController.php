<?php


namespace App\Account\Infrastructure\Controller;


use App\Account\Application\Command\AddServiceCommand;
use App\Shared\Infrastructure\ValidationErrorsFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/profile")
 */
class ProfileController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @var MessageBusInterface
     */
    private MessageBusInterface $messageBus;

    public function __construct(SerializerInterface $serializer, MessageBusInterface $messageBus)
    {
        $this->serializer = $serializer;
        $this->messageBus = $messageBus;
    }

    /**
     * @IsGranted("ROLE_EXPERT")
     *
     * @Route("/service/add", methods={"POST"}, name="add_service")
     */
    public function addService(Request $request): JsonResponse
    {
        try {
            $addServiceCommand = $this->serializer->deserialize(
                $request->getContent(),
                AddServiceCommand::class,
                'json'
            );
        } catch (ExceptionInterface $exception) {
            return new JsonResponse(sprintf('Wrong request payload: %s', $exception->getMessage()), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->messageBus->dispatch($addServiceCommand);
        } catch (ValidationFailedException $exception) {
            $errorsMessages = (new ValidationErrorsFetcher($exception->getViolations()))->getErrorsMessages();

            return new JsonResponse(['errors' => $errorsMessages], JsonResponse::HTTP_BAD_REQUEST);
        }

        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }
}