<?php


namespace App\Account\Infrastructure\Controller\Dto;


final class User
{
    private string $email;
    private array $roles;
    private Profile $profile;

    public function __construct(string $email, array $roles, Profile $profile)
    {
        $this->email = $email;
        $this->roles = $roles;
        $this->profile = $profile;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getProfile(): Profile
    {
        return $this->profile;
    }

}