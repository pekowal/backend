<?php


namespace App\Account\Infrastructure\Controller\Dto;


final class Profile
{
    private int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


}