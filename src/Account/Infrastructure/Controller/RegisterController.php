<?php


namespace App\Account\Infrastructure\Controller;

use App\Account\Application\Command\CreateAccountCommand;
use App\Account\Application\Command\CreateExpertAccountCommand;
use App\Account\Domain\Entity\CreateExpertAccessToken;
use App\Account\Infrastructure\Repository\CreateExpertAccessTokenRepository;
use App\Shared\Infrastructure\ValidationErrorsFetcher;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;

final class RegisterController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;
    private MessageBusInterface $messageBus;
    private CreateExpertAccessTokenRepository $accessTokenRepository;

    /**
     * RegisterController constructor.
     * @param EntityManagerInterface $entityManager
     * @param SerializerInterface $serializer
     * @param MessageBusInterface $messageBus
     * @param CreateExpertAccessTokenRepository $accessTokenRepository
     */
    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer, MessageBusInterface $messageBus, CreateExpertAccessTokenRepository $accessTokenRepository)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
        $this->messageBus = $messageBus;
        $this->accessTokenRepository = $accessTokenRepository;
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     *
     * @Route("/account/create-expert-link", methods={"POST"}, name="create_expert_link")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createCreateExpertLink(Request $request): JsonResponse
    {
        // CQRS? We cant do something and fetch
        $accessToken = new CreateExpertAccessToken();
        $this->entityManager->persist($accessToken);
        $this->entityManager->flush();

        return new JsonResponse($accessToken->getAccessToken());
    }

    /**
     * @Route("/register", methods={"POST"}, name="register_account")
     */
    public function registerUser(Request $request): JsonResponse
    {
        try {
            /** @var CreateAccountCommand $createAccountCommand */
            $createAccountCommand = $this->serializer->deserialize(
                $request->getContent(),
                CreateAccountCommand::class,
                'json'
            );
        } catch (ExceptionInterface $exception) {
            return new JsonResponse(sprintf('Wrong request payload: %s', $exception->getMessage()), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->messageBus->dispatch($createAccountCommand);
        } catch (ValidationFailedException $validationFailedException) {
            $errorsMessages = (new ValidationErrorsFetcher($validationFailedException->getViolations()))->getErrorsMessages();

            return new JsonResponse(['errors' => $errorsMessages], JsonResponse::HTTP_BAD_REQUEST);
        }

        return new JsonResponse();
    }

    /**
     * @Route("/register/expert/{accessToken}", methods={"POST"}, name="register_expert_account")
     *
     * @param Request $request
     * @param string $accessToken
     *
     * @return JsonResponse
     */
    public function registerExpert(Request $request, string $accessToken): JsonResponse
    {
        $accessToken = $this->accessTokenRepository->findOneBy(['accessToken' => $accessToken]);

        if (null === $accessToken) {
            return new JsonResponse(null, JsonResponse::HTTP_FORBIDDEN);
        }

        try {
            /** @var CreateExpertAccountCommand $createAccountCommand */
            $createAccountCommand = $this->serializer->deserialize(
                $request->getContent(),
                CreateExpertAccountCommand::class,
                'json'
            );
        } catch (ExceptionInterface $exception) {
            return new JsonResponse(sprintf('Wrong request payload: %s', $exception->getMessage()), JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $this->messageBus->dispatch($createAccountCommand);
        } catch (ValidationFailedException $validationFailedException) {
            $errorsMessages = (new ValidationErrorsFetcher($validationFailedException->getViolations()))->getErrorsMessages();

            return new JsonResponse(['errors' => $errorsMessages], JsonResponse::HTTP_BAD_REQUEST);
        }

        return new JsonResponse();
    }

}