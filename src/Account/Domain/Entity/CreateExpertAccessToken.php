<?php


namespace App\Account\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @todo rename
 * @ORM\Entity()
 */
class CreateExpertAccessToken
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * The access token for create expert account
     *
     * @ORM\Column(type="string")
     */
    private string $accessToken;

    public function __construct()
    {
        $this->accessToken = Uuid::v6()->toRfc4122();
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
}