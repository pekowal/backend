<?php


namespace App\Account\Domain\Entity;

use App\Calendar\Domain\Entity\Calendar;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Profile
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="Account", cascade={"persist"})
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     */
    private Account $account;

    /**
     * @ORM\OneToOne(targetEntity="Personality", cascade={"persist"})
     * @ORM\JoinColumn(name="personality_id", referencedColumnName="id")
     */
    private Personality $personality;

    /**
     * @ORM\OneToOne(targetEntity="App\Calendar\Domain\Entity\Calendar", cascade={"persist"})
     */
    private Calendar $calendar;

    public function __construct(Account $account, Personality $personality, Calendar $calendar)
    {
        $this->account = $account;
        $this->personality = $personality;
        $this->calendar = $calendar;
        $this->calendar->setProfile($this);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount(Account $account): void
    {
        $this->account = $account;
    }

    /**
     * @return Personality
     */
    public function getPersonality(): Personality
    {
        return $this->personality;
    }

    /**
     * @param Personality $personality
     */
    public function setPersonality(Personality $personality): void
    {
        $this->personality = $personality;
    }

    /**
     * @return Calendar
     */
    public function getCalendar(): Calendar
    {
        return $this->calendar;
    }

    /**
     * @param Calendar $calendar
     */
    public function setCalendar(Calendar $calendar): void
    {
        $this->calendar = $calendar;
    }
}