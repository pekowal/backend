<?php

namespace App\Account\Domain\Entity;

use App\Authorization\Infrastructure\Dictionary\AccountRolesDictionary;
use App\Authorization\Infrastructure\Traits\AccountApiTokenEntityTrait;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 */
class Account implements UserInterface
{
    use AccountApiTokenEntityTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Groups({"frontend"})
     *
     * @ORM\Column(type="string")
     */
    private string $email;

    /**
     * The hashed password
     *
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * @Groups({"frontend"})
     *
     * @ORM\Column(type="array")
     */
    private array $roles = [AccountRolesDictionary::ROLE_USER];

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $regulations = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $active = false;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $createdAt;

    public function __construct(string $email, string $password)
    {
        $this->email = $email;
        $this->password = $password;
        $this->createdAt = new DateTime();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return array|string[]
     */
    public function getRoles()
    {
        return $this->roles;
    }

    public function grantExpertRole(): void
    {
        $this->roles = [...$this->getRoles(), AccountRolesDictionary::ROLE_EXPERT];
    }

    public function grantAdminRole(): void
    {
        $this->roles = [...$this->getRoles(), AccountRolesDictionary::ROLE_ADMIN];
    }

    /**
     * @param array|string[] $roles
     */
    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    /**
     * @return bool
     */
    public function isRegulations(): bool
    {
        return $this->regulations;
    }

    /**
     * @param bool $regulations
     */
    public function setRegulations(bool $regulations): void
    {
        $this->regulations = $regulations;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername(): string
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}