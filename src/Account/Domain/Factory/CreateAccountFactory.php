<?php


namespace App\Account\Domain\Factory;


use App\Account\Application\Command\CreateAccountCommand;
use App\Account\Domain\Entity\Account;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateAccountFactory
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $userPasswordEncoder;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    public function createAccount(CreateAccountCommand $createAccountCommand): Account
    {
        //TODO remove from constructor
        $accountEntity = new Account($createAccountCommand->getEmail(), $createAccountCommand->getPlainPassword());
        $encodedPassword = $this->userPasswordEncoder->encodePassword($accountEntity, $createAccountCommand->getPlainPassword());
        $accountEntity->setPassword($encodedPassword);

        return $accountEntity;
    }
}