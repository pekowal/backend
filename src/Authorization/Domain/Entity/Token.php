<?php


namespace App\Authorization\Domain\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity()
 */
class Token
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * The authentication token
     *
     * @ORM\Column(type="string", length=36)
     */
    private string $token;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $expiredAt;

    public function __construct(DateTimeInterface $expiredAt)
    {
        $this->token = Uuid::v1()->toRfc4122();
        $this->createdAt = new DateTime();
        $this->expiredAt = $expiredAt;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return DateTimeInterface
     */
    public function getExpiredAt(): DateTimeInterface
    {
        return $this->expiredAt;
    }

}