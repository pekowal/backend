<?php


namespace App\Authorization\Infrastructure\Factory;


use App\Authorization\Application\AuthenticationTimeProvider;
use App\Authorization\Domain\Entity\Token;

class TokenEntityFactory
{
    /**
     * @var AuthenticationTimeProvider
     */
    private AuthenticationTimeProvider $authenticationTimeProvider;

    public function __construct(AuthenticationTimeProvider $authenticationTimeProvider)
    {
        $this->authenticationTimeProvider = $authenticationTimeProvider;
    }

    public function createToken(): Token
    {
        return new Token($this->authenticationTimeProvider->getDateTime());
    }

}