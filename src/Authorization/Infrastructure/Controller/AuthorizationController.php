<?php


namespace App\Authorization\Infrastructure\Controller;

use App\Account\Domain\Entity\Account;
use App\Authorization\Application\Command\LogoutAccountCommand;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/authorization")
 */
class AuthorizationController extends AbstractController
{
    /**
     * @var MessageBusInterface
     */
    private MessageBusInterface $messageBus;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    public function __construct(MessageBusInterface $messageBus, SerializerInterface $serializer)
    {
        $this->messageBus = $messageBus;
        $this->serializer = $serializer;
    }

    /**
     * @see LoginAuthenticator is executed before this action
     *
     * @Route("/login", methods={"POST"}, name="login")
     */
    public function loginAccount(Request $request): JsonResponse
    {
        /** @var Account $user */
        $user = $this->getUser();

        return new JsonResponse($user->getToken()->getToken());
    }

    /**
     * @Route("/logout", methods={"POST"}, name="logout")
     */
    public function logout(Request $request): JsonResponse
    {
        if (null === $this->getUser()) {
            return new JsonResponse(null, JsonResponse::HTTP_BAD_REQUEST);
        }

        $this->messageBus->dispatch(new LogoutAccountCommand($this->getUser()->getId()));

        return new JsonResponse();
    }

}