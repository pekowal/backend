<?php


namespace App\Authorization\Infrastructure\Traits;


use Doctrine\ORM\Mapping as ORM;
use App\Authorization\Domain\Entity\Token;

trait AccountApiTokenEntityTrait
{
    /**
     * @ORM\OneToOne(targetEntity="App\Authorization\Domain\Entity\Token", cascade={"persist"})
     * @ORM\JoinColumn(name="token_id", referencedColumnName="id")
     */
    private ?Token $token;

    /**
     * @return Token|null
     */
    public function getToken(): ?Token
    {
        return $this->token;
    }

    /**
     * @param Token|null $token
     */
    public function setToken(?Token $token): void
    {
        $this->token = $token;
    }

}