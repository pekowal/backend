<?php


namespace App\Authorization\Infrastructure\Dictionary;


class AuthorizationRoutesNames
{
    const LOGIN_ROUTE_NAME = 'login';
    const REGISTER_ROUTE_NAME = 'register_account';
}