<?php


namespace App\Authorization\Infrastructure\Dictionary;


class AccountRolesDictionary
{
    const ROLE_USER = 'ROLE_USER';
    const ROLE_EXPERT = 'ROLE_EXPERT';
    const ROLE_ADMIN = 'ROLE_ADMIN';
}