<?php


namespace App\Authorization\Infrastructure\Repository;

use App\Authorization\Domain\Entity\Token;
use App\Shared\Infrastructure\AbstractEntityRepository;
use App\Shared\Infrastructure\EntityRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

final class TokenRepository extends AbstractEntityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, Token::class, $entityManager);
        $this->entityManager = $entityManager;
    }
}