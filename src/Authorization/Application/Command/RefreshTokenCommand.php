<?php


namespace App\Authorization\Application\Command;


use App\Account\Domain\Entity\Account;

final class RefreshTokenCommand
{
    private int $accountId;

    /**
     * RefreshTokenCommand constructor.
     * @param int $accountId
     */
    public function __construct(int $accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * @return int
     */
    public function getAccountId(): int
    {
        return $this->accountId;
    }
}