<?php


namespace App\Authorization\Application\Command;


final class LogoutAccountCommand
{
    private int $accountId;

    public function __construct(int $accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * @return int
     */
    public function getAccountId(): int
    {
        return $this->accountId;
    }
}