<?php


namespace App\Authorization\Application;


use DateTime;
use Exception;

class AuthenticationTimeProvider
{
    /**
     * Authentication time after login see variable in .env
     */
    private int $authenticationTime;

    public function __construct(int $authenticationTime)
    {
        $this->authenticationTime = $authenticationTime;
    }

    /**
     * @return DateTime
     *
     * @throws Exception
     */
    public function getDateTime(): DateTime
    {
        return new DateTime(sprintf('+%s minutes', $this->authenticationTime));
    }
}