<?php

namespace App\Authorization\Application;

use App\Account\Domain\Entity\Account;
use App\Account\Infrastructure\Repository\AccountRepository;
use App\Authorization\Application\Command\RefreshTokenCommand;
use App\Authorization\Infrastructure\Dictionary\AuthorizationRoutesNames;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;

final class LoginAuthenticator extends AbstractAuthenticator
{
    /**
     * @var AccountRepository
     */
    private AccountRepository $accountRepository;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @var MessageBusInterface
     */
    private MessageBusInterface $messageBus;

    /**
     * LoginAuthenticator constructor.
     * @param AccountRepository $accountRepository
     * @param SerializerInterface $serializer
     * @param MessageBusInterface $messageBus
     */
    public function __construct(AccountRepository $accountRepository, SerializerInterface $serializer, MessageBusInterface $messageBus)
    {
        $this->accountRepository = $accountRepository;
        $this->serializer = $serializer;
        $this->messageBus = $messageBus;
    }

    public function supports(Request $request): ?bool
    {
        return AuthorizationRoutesNames::LOGIN_ROUTE_NAME === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    public function authenticate(Request $request): PassportInterface
    {
        /** @var LoginValueObject $loginValueObject */
        $loginValueObject = $this->serializer->deserialize($request->getContent(), LoginValueObject::class, 'json');

        /** @var Account $account */
        $account = $this->accountRepository->findOneBy(['email' => $loginValueObject->getEmail()]);

        if (null === $account)
            throw new UsernameNotFoundException(sprintf('User with email "%s" does not exist', $loginValueObject->getEmail()));

        return new Passport(
            $account,
            new PasswordCredentials($loginValueObject->getPlainPassword())
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        $refreshTokenCommand = new RefreshTokenCommand($token->getUser()->getId());

        $this->messageBus->dispatch($refreshTokenCommand);
        // on success, let the request continue
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            // you may want to customize or obfuscate the message first
            'error' => strtr($exception->getMessageKey(), $exception->getMessageData())

            // or to translate this message
//             $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_BAD_REQUEST);
    }
}