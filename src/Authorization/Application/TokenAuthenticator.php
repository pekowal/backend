<?php

namespace App\Authorization\Application;

use App\Account\Infrastructure\Repository\AccountRepository;
use App\Authorization\Application\Credentials\TokenCredentials;
use DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

final class TokenAuthenticator extends AbstractAuthenticator
{
    private string $apiTokenHeaderName;

    /**
     * @var AccountRepository
     */
    private AccountRepository $accountRepository;

    /**
     * TokenAuthenticator constructor.
     * @param string $apiTokenHeaderName
     * @param AccountRepository $accountRepository
     */
    public function __construct(string $apiTokenHeaderName, AccountRepository $accountRepository)
    {
        $this->apiTokenHeaderName = $apiTokenHeaderName;
        $this->accountRepository = $accountRepository;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = [
            // you might translate this message
            'message' => 'Authentication Required'
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supports(Request $request): ?bool
    {
        return $request->headers->has($this->apiTokenHeaderName);
    }

    public function authenticate(Request $request): PassportInterface
    {
        $token = $request->headers->get($this->apiTokenHeaderName);

        $account = $this->accountRepository->findOneByApiToken($token);

        if (null === $account) {
            throw new UsernameNotFoundException(sprintf('User with token %s not found', $token));
        }

        if ($account->getToken()->getExpiredAt() < new DateTime()) {
            throw new CustomUserMessageAuthenticationException('API token expired.');
        }

        return new SelfValidatingPassport($account);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            // you may want to customize or obfuscate the message first
            'error' => strtr($exception->getMessageKey(), $exception->getMessageData())

            // or to translate this message
//             $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }
}