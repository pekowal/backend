<?php


namespace App\Authorization\Application\Credentials;


use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CredentialsInterface;

final class TokenCredentials implements CredentialsInterface
{
    private string $apiToken;

    public function __construct(string $apiToken)
    {
        $this->apiToken = $apiToken;
    }

    public function isResolved(): bool
    {
        return true;
    }
}