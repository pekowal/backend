<?php

namespace App\Authorization\Application\CommandHandler;

use App\Account\Infrastructure\Repository\AccountRepository;
use App\Authorization\Application\Command\LogoutAccountCommand;
use App\Authorization\Infrastructure\Repository\TokenRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class LogoutAccountCommandHandler implements MessageHandlerInterface
{
    private AccountRepository $accountRepository;

    private TokenRepository $tokenRepository;

    public function __construct(AccountRepository $accountRepository, TokenRepository $tokenRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->tokenRepository = $tokenRepository;
    }

    public function __invoke(LogoutAccountCommand $logoutAccountCommand)
    {
        $account = $this->accountRepository->find($logoutAccountCommand->getAccountId());
        $token = $account->getToken();
        $account->setToken(null);
        $this->tokenRepository->remove($token);
    }
}