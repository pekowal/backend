<?php


namespace App\Authorization\Application\CommandHandler;

use App\Account\Infrastructure\Repository\AccountRepository;
use App\Authorization\Application\Command\RefreshTokenCommand;
use App\Authorization\Infrastructure\Factory\TokenEntityFactory;
use App\Authorization\Infrastructure\Repository\TokenRepository;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class RefreshTokenCommandHandler implements MessageHandlerInterface
{
    /**
     * @var AccountRepository
     */
    private AccountRepository $accountRepository;

    /**
     * @var TokenRepository
     */
    private TokenRepository $tokenRepository;
    /**
     * @var TokenEntityFactory
     */
    private TokenEntityFactory $tokenEntityFactory;

    /**
     * RefreshTokenCommandHandler constructor.
     *
     * @param AccountRepository $accountRepository
     * @param TokenRepository $tokenRepository
     * @param TokenEntityFactory $tokenEntityFactory
     */
    public function __construct(AccountRepository $accountRepository, TokenRepository $tokenRepository,  TokenEntityFactory $tokenEntityFactory)
    {
        $this->accountRepository = $accountRepository;
        $this->tokenRepository = $tokenRepository;
        $this->tokenEntityFactory = $tokenEntityFactory;
    }

    /**
     * @throws EntityNotFoundException
     *
     * @param RefreshTokenCommand $createTokenCommand
     */
    public function __invoke(RefreshTokenCommand $createTokenCommand)
    {
        $account = $this->accountRepository->find($createTokenCommand->getAccountId());

        if ($account->getToken()) $this->tokenRepository->remove($account->getToken());

        $account->setToken($this->tokenEntityFactory->createToken());
        $this->accountRepository->persist($account);
    }
}