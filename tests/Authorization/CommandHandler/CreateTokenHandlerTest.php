<?php


namespace App\Tests\Authorization\CommandHandler;


use App\Account\Domain\Entity\Account;
use App\Account\Infrastructure\Repository\AccountRepository;
use App\Authorization\Application\Command\RefreshTokenCommand;
use App\Authorization\Application\CommandHandler\RefreshTokenCommandHandler;
use App\Authorization\Infrastructure\Repository\TokenRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CreateTokenHandlerTest extends TestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    private $kernel;

    protected function setUp(): void
    {
        $this->kernel = self::bootKernel();

        $this->entityManager = $this->kernel->getContainer()->get('doctrine')->getManager();
    }

    public function testSearchByName()
    {
        $account = new Account('test@email.pl', 'password');

        $this->entityManager->persist($account);
        $this->entityManager->flush();


        $handler = new RefreshTokenCommandHandler(
            $this->kernel->getContainer()->get(AccountRepository::class),
            $this->kernel->getContainer()->get(TokenRepository::class)
        );

        $handler(new RefreshTokenCommand($account->getId()));

        $this->entityManager->refresh($account);
        $this->assertNotEmpty($account->getToken());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}