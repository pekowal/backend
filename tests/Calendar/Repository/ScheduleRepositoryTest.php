<?php


namespace App\Tests\Calendar\Repository;


use App\Account\Domain\Entity\Account;
use App\Account\Domain\Entity\Personality;
use App\Account\Domain\Entity\Profile;
use App\Calendar\Application\Command\ScheduleAddCommand;
use App\Calendar\Domain\Entity\Calendar;
use App\Calendar\Domain\Entity\Schedule;
use App\Calendar\Infrastructure\Repository\ScheduleRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\RowNumberOverFunction;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ScheduleRepositoryTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testSearchByName()
    {
        /** @var ScheduleRepository $scheduleRepo */
        $scheduleRepo = $this->entityManager
            ->getRepository(Schedule::class);
        $calendar = new Calendar();

        $profile = new Profile(
            new Account(
                'email@email.pl',
                '123'
            ),
            new Personality(
                'first name',
                'last name'
            ),
            $calendar
        );
        $this->entityManager->persist($profile);

        $schedule = new Schedule($calendar, 0 ,'name', 11.5, 12.5);
        $this->entityManager->persist($schedule);
        $this->entityManager->flush();

        $scheduleFromRepo = $scheduleRepo->find($schedule->getId());
        $this->assertNotEmpty($scheduleFromRepo);

        dump($scheduleFromRepo);

        $scheduleAddCommand = new ScheduleAddCommand();
        $scheduleAddCommand->setStartDate('11:30');
        $scheduleAddCommand->setEndDate('12:30');
        $scheduleAddCommand->setProfileId($profile->getId());
        $scheduleAddCommand->setWeekDay(0);

        $schedule = $scheduleRepo->fetchSchedule($scheduleAddCommand);

        $this->assertNotEmpty($schedule);

        $scheduleAddCommand->setStartDate('10:00');
        $schedule = $scheduleRepo->fetchSchedule($scheduleAddCommand);

        $this->assertNotEmpty($schedule);

        $scheduleAddCommand->setStartDate('12:00');
        $scheduleAddCommand->setEndDate('13:00');
        $schedule = $scheduleRepo->fetchSchedule($scheduleAddCommand);

        $this->assertNotEmpty($schedule);

        $scheduleAddCommand->setStartDate('12:30');
        $scheduleAddCommand->setEndDate('14:30');
        $schedule = $scheduleRepo->fetchSchedule($scheduleAddCommand);

        $this->assertNull($schedule);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}